﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace tpweb3.Dao
{
    public static class AlumnoDao
    {
        public static Alumno mostrarAlAlumno(String email,String password)
        {
            Alumno alumno = new Alumno();
            TP_20191CEntities context = new TP_20191CEntities();
            var alu = from a in context.Alumno where a.Email == email && a.Password == password select a;
            alumno = alu.FirstOrDefault();
            
            return alumno;
        }

        public static Alumno mostrarAlumnoPorDni(int id)
        {
            Alumno alumno = new Alumno();
            TP_20191CEntities context = new TP_20191CEntities();
            var alu = from a in context.Alumno where a.IdAlumno==id select a;
            alumno = alu.FirstOrDefault();
            
            return alumno;
        }
        public static List<Alumno> mostrarTodosLosAlumnos()
        {
            List<Alumno> alumnos = new List<Alumno>();
            TP_20191CEntities context = new TP_20191CEntities();
            var alus = context.Alumno.OrderByDescending(x => x.PuntosTotales).
                       ThenByDescending(x => x.CantidadRespuestasCorrectas).
                       ThenByDescending(x => x.CantidadMejorRespuesta).Select(x => x);
                       
            alumnos = alus.ToList();
           

            return alumnos;
        }
        public static Alumno traerAlumnoPorId(int id) {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Alumno.Where(a => a.IdAlumno == id).FirstOrDefault();
        }
    }
}