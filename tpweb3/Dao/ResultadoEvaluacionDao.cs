﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tpweb3.Dao
{
    public static class ResultadoEvaluacionDao
    {
        public static String traerNombreResultado(int param)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            ResultadoEvaluacion r = ctx.ResultadoEvaluacion.Find(param);
            return r.Resultado;
        }

        public static ResultadoEvaluacion traerResultado(RespuestaAlumno respuesta)
        {
            TP_20191CEntities context = new TP_20191CEntities();
            ResultadoEvaluacion resultado = new ResultadoEvaluacion();

            var res = from re in context.ResultadoEvaluacion
                      join r in context.RespuestaAlumno on re.IdResultadoEvaluacion equals r.ResultadoEvaluacion.IdResultadoEvaluacion
                      where r.IdRespuestaAlumno == respuesta.IdRespuestaAlumno 
                      select re;

            resultado = res.FirstOrDefault();
            return resultado;
        }
    }
}