﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tpweb3.Models
{
    public class RespuestaAlumnoForm
    {
        [AllowHtml]
        [Required(ErrorMessage = "La respuesta es requerida")]
        public string respuesta { set; get; }

    }
}